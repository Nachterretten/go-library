package db

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"library/internal/config"
)

func NewSqlDB(dbConf config.DB) (*sqlx.DB, error) {
	var dsn string
	var err error
	var dbRaw *sqlx.DB

	switch dbConf.Driver {
	case "postgres":
		dsn = fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable",
			dbConf.Host, dbConf.Port, dbConf.User, dbConf.Password, dbConf.DBName)
	default:
		return nil, fmt.Errorf("usupported database driver: %s", dbConf.Driver)
	}
	dbRaw, err = sqlx.Connect(dbConf.Driver, dsn)
	if err != nil {
		return nil, err
	}
	return dbRaw, err
}
