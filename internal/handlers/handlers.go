package handlers

import (
	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
	"library/internal/model"
	"net/http"
	"strconv"

	_ "library/docs"
)

// @Summary CreateAuthor
// @Tags author
// @Description Add a new author
// @Accept json
// @Produce json
// @Param input body model.Author true "Author object"
// @Success 201 {integer} integer 1
// @Failure 400 {object} errorResponse
// @Failure 404 {object} errorResponse
// @Router /author [post]
func (s *APIServer) CreateAuthor(c *gin.Context) {
	var author model.Author
	if err := c.ShouldBindJSON(&author); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	if err := s.libraryService.CreateAuthor(author); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	c.Status(http.StatusCreated)
}

// @Summary GetAllAuthors
// @Tags author
// @Description Get all authors
// @Produce json
// @Success 200 {array} model.Author
// @Failure 400 {object} errorResponse
// @Failure 500 {object} errorResponse
// @Router /authors [get]
func (s *APIServer) GetAllAuthors(c *gin.Context) {
	authors, err := s.libraryService.GetAllAuthors()
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Failed to get authors"})
		return
	}
	c.JSON(http.StatusOK, authors)
}

// @Summary GetAuthorsWithBooks
// @Tags author
// @Description Get authors with their books by author ID
// @Produce json
// @Param authorID path int true "Author ID"
// @Success 200 {array} model.Book
// @Failure 400 {object} errorResponse
// @Failure 500 {object} errorResponse
// @Router /author/{authorID} [get]
func (s *APIServer) GetAuthorsWithBooks(c *gin.Context) {
	authorID, err := strconv.Atoi(c.Param("authorID"))
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid author index"})
		return
	}

	books, err := s.libraryService.GetBooksByAuthor(authorID)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get books by author"})
		return
	}
	c.JSON(http.StatusOK, books)
}

// @Summary GetAllBooks
// @Tags book
// @Description Get all books
// @Produce json
// @Success 200 {array} model.Book
// @Failure 500 {object} errorResponse
// @Router /books [get]
func (s *APIServer) GetAllBooks(c *gin.Context) {
	books, err := s.libraryService.GetAllBooks()
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to get books"})
		return
	}
	c.JSON(http.StatusOK, books)
}

// @Summary AddBook
// @Tags book
// @Description Add a new book
// @Accept json
// @Produce json
// @Param input body model.Book true "Book object"
// @Success 200 {object} successResponse
// @Failure 400 {object} errorResponse
// @Failure 500 {object} errorResponse
// @Router /book [post]
func (s *APIServer) AddBook(c *gin.Context) {
	var book model.Book
	if err := c.ShouldBindJSON(&book); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid book data"})
		return
	}

	if err := s.libraryService.AddBook(book); err != nil {
		if errors.Is(err, errors.New("specified author does not exist")) {
			c.JSON(http.StatusBadRequest, gin.H{"error": "Specified author does not exist"})
		} else {
			c.JSON(http.StatusInternalServerError, gin.H{"error": "Failed to add book"})
		}
		return
	}
	c.JSON(http.StatusOK, gin.H{"message": "Book added"})
}

// @Summary BorrowBook
// @Tags book
// @Description Borrow a book by book ID and user ID
// @Produce json
// @Param bookID path int true "Book ID"
// @Param userID path int true "User ID"
// @Success 200 {object} successResponse
// @Failure 400 {object} errorResponse
// @Failure 500 {object} errorResponse
// @Router /book/{bookID}/borrow/{userID} [post]
func (s *APIServer) BorrowBook(c *gin.Context) {
	// Получаем параметры из запроса
	bookID := c.Param("bookID")
	userID := c.Param("userID")

	// Преобразуем параметры в тип int
	bookIDInt, err := strconv.Atoi(bookID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid book ID"})
		return
	}

	userIDInt, err := strconv.Atoi(userID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user ID"})
		return
	}

	// Вызываем метод BorrowBook из репозитория
	err = s.libraryService.BorrowBook(bookIDInt, userIDInt)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Book successfully borrowed"})
}

// @Summary ReturnBook
// @Tags book
// @Description Return a borrowed book by book ID and user ID
// @Produce json
// @Param bookID path int true "Book ID"
// @Param userID path int true "User ID"
// @Success 200 {object} successResponse
// @Failure 400 {object} errorResponse
// @Failure 500 {object} errorResponse
// @Router /book/return/{bookID}/user/{userID} [post]
func (s *APIServer) ReturnBook(c *gin.Context) {
	// Получаем параметры из запроса
	bookID := c.Param("bookID")
	userID := c.Param("userID")

	// Преобразуем параметры в тип int
	bookIDInt, err := strconv.Atoi(bookID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid book ID"})
		return
	}

	userIDInt, err := strconv.Atoi(userID)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Invalid user ID"})
		return
	}

	// Вызываем метод ReturnBook из репозитория
	err = s.libraryService.ReturnBook(bookIDInt, userIDInt)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{"error": err.Error()})
		return
	}

	c.JSON(http.StatusOK, gin.H{"message": "Book successfully returned"})
}

// authorResponse represents a response containing an author.
// swagger:response authorResponse
type authorResponse struct {
	// in: body
	Body model.Author
}

// authorWithBooksResponse represents a response containing an author with their books.
// swagger:response authorWithBooksResponse
type authorWithBooksResponse struct {
	// in: body
	Body struct {
		Author model.Author `json:"author"`
		Books  []model.Book `json:"books"`
	}
}

// bookResponse represents a response containing a book.
// swagger:response bookResponse
type bookResponse struct {
	// in: body
	Body model.Book
}

// successResponse represents a successful response.
// swagger:response successResponse
type successResponse struct {
	// in: body
	Body struct {
		Message string `json:"message"`
	}
}

// errorResponse represents an error response.
// swagger:response errorResponse
type errorResponse struct {
	// in: body
	Body struct {
		Error string `json:"error"`
	}
}
