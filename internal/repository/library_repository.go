package repository

import (
	"database/sql"
	"github.com/brianvoe/gofakeit/v6"
	"github.com/jmoiron/sqlx"
	"github.com/lib/pq"
	"github.com/pkg/errors"
	"library/internal/model"
	"log"
	"time"
)

type LibraryRepositoryInterface interface {
	CreateAuthor(author model.Author) error
	GetBooksByAuthor(authorID int) ([]model.Book, error)
	GetAllBooks() ([]model.Book, error)
	AddBook(book model.Book) error
	BorrowBook(bookID, userID int) error
	GetAllAuthors() ([]model.Author, error)
	ReturnBook(bookID, userID int) error
	CheckAuthorsExistence() (bool, error)
	CheckBooksExistence() (bool, error)
	AddFakeAuthors(count int) error
	AddFakeBooks(count int) error
}
type LibraryRepository struct {
	db *sqlx.DB
}

func NewLibraryRepository(db *sqlx.DB) *LibraryRepository {
	return &LibraryRepository{
		db: db,
	}
}

func (l *LibraryRepository) CheckBooksExistence() (bool, error) {
	var count int
	err := l.db.Get(&count, "SELECT COUNT(*) FROM library.books ")
	if err != nil {
		return false, err
	}
	return count > 0, nil
}

func (l *LibraryRepository) CheckAuthorsExistence() (bool, error) {
	var count int
	err := l.db.Get(&count, "SELECT COUNT(*) FROM library.authors")
	if err != nil {
		return false, err
	}
	return count > 0, nil
}

func (l *LibraryRepository) GetAllAuthors() ([]model.Author, error) {
	// SQL-запрос для получения всех авторов и их книг
	query := `SELECT 
				  a.author_id, 
				  a.name, 
				  b.book_id, 
				  b.title 
			  FROM 
				  library.Authors AS a 
			  LEFT JOIN 
				  library.Author_Books AS ab ON a.author_id = ab.author_id
			  LEFT JOIN 
				  library.Books AS b ON ab.book_id = b.book_id`

	// Выполнение запроса к базе данных
	rows, err := l.db.Query(query)
	if err != nil {
		log.Println("Failed to execute the query:", err)
		return nil, err
	}
	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {

		}
	}(rows)

	// Создание мапы для хранения авторов и их книг
	authorsMap := make(map[int]model.Author)

	// Итерация по результирующим строкам
	for rows.Next() {
		// Чтение значений из строки результата
		var authorID int
		var authorName string
		var nullableBookID sql.NullInt64
		var nullableBookTitle sql.NullString

		err := rows.Scan(&authorID, &authorName, &nullableBookID, &nullableBookTitle)
		if err != nil {
			log.Println("Failed to scan row values:", err)
			return nil, err
		}

		// Проверка наличия значения NULL для столбца "book_id"
		var bookID int
		if nullableBookID.Valid {
			bookID = int(nullableBookID.Int64)
		} else {
			// Обрабатываем случай, когда значение столбца "book_id" равно NULL
			bookID = 0 // или любое другое значение, которое указывает на отсутствие ID книги
		}

		// Проверка наличия значения NULL для столбца "title"
		var bookTitle string
		if nullableBookTitle.Valid {
			bookTitle = nullableBookTitle.String
		} else {
			// Обрабатываем случай, когда значение столбца "title" равно NULL
			bookTitle = "" // или любое другое значение по умолчанию для заголовка книги
		}
		// Проверка, существует ли уже автор в мапе
		author, ok := authorsMap[authorID]
		if !ok {
			// Если автора еще нет, создаем новый экземпляр
			author = model.Author{
				AuthorID: authorID,
				Name:     authorName,
				Books:    []model.Book{},
			}
		}

		// Создание экземпляра книги и добавление книги в автора
		book := model.Book{
			BookID:   bookID,
			Title:    bookTitle,
			AuthorID: authorID,
		}
		author.Books = append(author.Books, book)

		// Обновление мапы авторов
		authorsMap[authorID] = author
	}

	// Преобразование мапы в слайс авторов
	var authors []model.Author
	for _, author := range authorsMap {
		authors = append(authors, author)
	}

	// Обработка возможной ошибки после итерации по строкам результата
	err = rows.Err()
	if err != nil {
		log.Println("Error occurred during iteration over result rows:", err)
		return nil, err
	}

	// Возвращение слайса авторов
	return authors, nil
}

func (l *LibraryRepository) CreateAuthor(author model.Author) error {
	query := "INSERT INTO library.Authors (name) VALUES ($1)"
	_, err := l.db.Exec(query, author.Name)
	if err != nil {
		log.Println("Failed to create author: ", err)
		return errors.Wrap(err, "failed to create author")
	}
	return nil
}

func (l *LibraryRepository) GetBooksByAuthor(authorID int) ([]model.Book, error) {
	query := `
		SELECT
			b.book_id,
			b.title,
			b.is_available
		FROM
			library.books AS b
		WHERE
			b.author_id = $1
		ORDER BY
			b.book_id
	`

	rows, err := l.db.Query(query, authorID)
	if err != nil {
		log.Println("Failed to get books by author: ", err)
		return nil, errors.Wrap(err, "failed to get books by author")
	}
	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {

		}
	}(rows)

	books := make([]model.Book, 0)
	for rows.Next() {
		var (
			bookID        int
			bookTitle     string
			bookAvailable bool
			authorID      int
			authorName    string
		)

		err := rows.Scan(&bookID, &bookTitle, &bookAvailable, &authorID, &authorName)
		if err != nil {
			log.Println("Failed to scan row: ", err)
			return nil, errors.Wrap(err, "failed to scan row")
		}

		book := model.Book{
			BookID:      bookID,
			Title:       bookTitle,
			IsAvailable: bookAvailable,
			AuthorID:    authorID,
		}

		books = append(books, book)
	}

	return books, nil
}

func (l *LibraryRepository) BorrowBook(bookID, userID int) error {
	// Проверяем, что книга доступна
	book, err := l.GetBookByID(bookID)
	if err != nil {
		return errors.Wrap(err, "error to get book")
	}

	if !book.IsAvailable {
		return errors.New("book is not available for rent")
	}

	// Обновляем статус доступности книги
	updateBookAvailabilityQuery := `
		UPDATE library.books
		SET is_available = false
		WHERE book_id = $1
	`
	_, err = l.db.Exec(updateBookAvailabilityQuery, bookID)
	if err != nil {
		return errors.Wrap(err, "error updating the book availability status")
	}

	// Добавляем запись об аренде в базу данных
	addBorrowQuery := `
		INSERT INTO library.borrowed_books (book_id, user_id, borrow_date)
		VALUES ($1, $2, $3)
	`
	_, err = l.db.Exec(addBorrowQuery, bookID, userID, time.Now())
	if err != nil {
		return errors.Wrap(err, "error when adding a rental record")
	}

	// Обновляем запись пользователя в базе данных
	user, err := l.GetUserByID(userID)
	if err != nil {
		return errors.Wrap(err, "error receiving the user")
	}

	user.RentedBooksIDs = append(user.RentedBooksIDs, bookID)

	updateUserQuery := `
		UPDATE library.users
		SET rented_books_ids = $1
		WHERE user_id = $2
	`
	rentedBooksIDs := pq.Array(user.RentedBooksIDs)

	_, err = l.db.Exec(updateUserQuery, rentedBooksIDs, userID)
	if err != nil {
		return errors.Wrap(err, "error updating user information")
	}

	return nil
}

func (l *LibraryRepository) GetAllBooks() ([]model.Book, error) {
	query := "SELECT b.book_id, b.title, b.is_available, a.author_id, a.name FROM library.books AS b INNER JOIN library.authors AS a ON b.author_id = a.author_id ORDER BY b.book_id"
	rows, err := l.db.Query(query)
	if err != nil {
		log.Println("Failed to egt all books: ", err)
	}
	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {

		}
	}(rows)

	books := make([]model.Book, 0)
	for rows.Next() {
		var (
			bookID        int
			bookTitle     string
			bookAvailable bool
			authorID      int
			authorName    string
		)

		err := rows.Scan(&bookID, &bookTitle, &bookAvailable, &authorID, &authorName)
		if err != nil {
			log.Println("Failed to scan row: ", err)
		}

		author := model.Author{
			AuthorID: authorID,
			Name:     authorName,
		}

		book := model.Book{
			BookID:      bookID,
			Title:       bookTitle,
			IsAvailable: bookAvailable,
			AuthorID:    author.AuthorID,
		}

		books = append(books, book)
	}
	return books, nil
}

func (l *LibraryRepository) CheckAuthorExists(authorID int) (bool, error) {
	query := "SELECT EXISTS(SELECT 1 FROM library.authors WHERE author_id = $1)"
	var exists bool
	err := l.db.QueryRow(query, authorID).Scan(&exists)
	if err != nil {
		return false, errors.Wrap(err, "failed to check author existence")
	}
	return exists, nil
}

func (l *LibraryRepository) AddBook(book model.Book) error {
	// проверяем существование автора
	authorExists, err := l.CheckAuthorExists(book.AuthorID)
	if err != nil {
		return errors.Wrap(err, "failed to check author existence")
	}
	if !authorExists {
		return errors.New("specified author does not exist")
	}

	// запрос на добавление книги
	query := `INSERT INTO library.books (title, author_id, is_available)
    VALUES ($1, $2, $3)`
	_, err = l.db.Exec(query, book.Title, book.AuthorID, book.IsAvailable)
	if err != nil {
		return errors.Wrap(err, "failed to add book")
	}
	return nil
}

func (l *LibraryRepository) GetUserByID(userID int) (*model.User, error) {
	query := `
	SELECT 
		u.user_id,
		u.name
	FROM
	    library.users AS u
	WHERE 
		u.user_id = $1
	`

	var (
		userID2 int
		name    string
	)

	err := l.db.QueryRow(query, userID).Scan(&userID2, &name)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("user not found")
		}
		return nil, errors.Wrap(err, "failed to get user")
	}

	user := &model.User{
		UserID: userID2,
		Name:   name,
	}

	// Получаем арендованные книги пользователя
	rentedBooks, err := l.GetRentedBooksByUserID(userID)
	if err != nil {
		return nil, errors.Wrap(err, "failed to get rented books")
	}
	user.RentedBooks = rentedBooks

	return user, nil
}

func (l *LibraryRepository) GetRentedBooksByUserID(userID int) ([]model.Book, error) {
	query := `
	SELECT
		b.book_id,
		b.title,
		b.author_id,
		b.is_available
	FROM
		library.books AS b
	INNER JOIN
		library.borrowed_books AS bb ON b.book_id = bb.book_id
	WHERE
		bb.user_id = $1
	`

	rows, err := l.db.Query(query, userID)
	if err != nil {
		log.Println("Failed to get rented books by user ID: ", err)
		return nil, errors.Wrap(err, "failed to get rented books by user ID")
	}
	defer func(rows *sql.Rows) {
		err := rows.Close()
		if err != nil {
			log.Println("Failed to close rows: ", err)
		}
	}(rows)

	books := make([]model.Book, 0)
	for rows.Next() {
		var (
			bookID      int
			title       string
			authorID    int
			isAvailable bool
		)

		err := rows.Scan(&bookID, &title, &authorID, &isAvailable)
		if err != nil {
			log.Println("Failed to scan row: ", err)
			return nil, errors.Wrap(err, "failed to scan row")
		}

		book := model.Book{
			BookID:      bookID,
			Title:       title,
			AuthorID:    authorID,
			IsAvailable: isAvailable,
		}

		books = append(books, book)
	}

	return books, nil
}

func (l *LibraryRepository) GetBookByID(bookID int) (*model.Book, error) {
	query := `
	SELECT 
		b.book_id,
		b.title,
		b.is_available,
		b.author_id,
		a.name
	FROM
	    library.books AS b
	INNER JOIN 
		library.authors AS a ON b.author_id = a.author_id
	WHERE 
		b.book_id = $1
	`

	var (
		bookID2       int
		bookTitle     string
		bookAvailable bool
		authorID      int
		authorName    string
	)

	err := l.db.QueryRow(query, bookID).Scan(&bookID2, &bookTitle, &bookAvailable, &authorID, &authorName)
	if err != nil {
		if err == sql.ErrNoRows {
			return nil, errors.New("book not found")
		}
		return nil, errors.Wrap(err, "failed to get book")
	}

	author := model.Author{
		AuthorID: authorID,
		Name:     authorName,
	}

	book := model.Book{
		BookID:      bookID2,
		Title:       bookTitle,
		IsAvailable: bookAvailable,
		AuthorID:    author.AuthorID,
	}
	return &book, err
}

func (l *LibraryRepository) ReturnBook(bookID, userID int) error {
	// Обновляем статус доступности книги
	updateBookAvailabilityQuery := `
		UPDATE library.books
		SET is_available = true
		WHERE book_id = $1
	`
	_, err := l.db.Exec(updateBookAvailabilityQuery, bookID)
	if err != nil {
		return errors.Wrap(err, "ошибка при обновлении статуса доступности книги")
	}

	// Удаляем запись об аренде из базы данных
	deleteBorrowQuery := `
		DELETE FROM library.borrowed_books
		WHERE book_id = $1 AND user_id = $2
	`
	_, err = l.db.Exec(deleteBorrowQuery, bookID, userID)
	if err != nil {
		return errors.Wrap(err, "ошибка при удалении записи об аренде")
	}

	// Обновляем запись пользователя в базе данных
	user, err := l.GetUserByID(userID)
	if err != nil {
		return errors.Wrap(err, "ошибка при получении пользователя")
	}

	// Удаляем ID арендованной книги из списка арендованных книг пользователя
	updatedRentedBooksIDs := make([]int, 0, len(user.RentedBooksIDs))
	for _, rentedBookID := range user.RentedBooksIDs {
		if rentedBookID != bookID {
			updatedRentedBooksIDs = append(updatedRentedBooksIDs, rentedBookID)
		}
	}

	updateUserQuery := `
		UPDATE library.users
		SET rented_books_ids = $1
		WHERE user_id = $2
	`
	rentedBooksIDs := pq.Array(updatedRentedBooksIDs)

	_, err = l.db.Exec(updateUserQuery, rentedBooksIDs, userID)
	if err != nil {
		return errors.Wrap(err, "ошибка при обновлении информации о пользователе")
	}

	return nil
}

func (l *LibraryRepository) AddFakeAuthors(count int) error {
	tx, err := l.db.Begin()
	if err != nil {
		return err
	}

	for i := 0; i < count; i++ {
		author := model.Author{
			Name: gofakeit.Name(),
		}

		// добавляем автора в таблицу
		err = tx.QueryRow("INSERT INTO library.authors (name) VALUES ($1) RETURNING author_id", author.Name).Scan(&author.AuthorID)
		if err != nil {
			tx.Rollback()
			return err
		}
	}
	return tx.Commit()
}

func (l *LibraryRepository) GetAuthorsCount() (int, error) {
	var count int
	err := l.db.Get(&count, "SELECT COUNT (*) FROM library.Authors")
	if err != nil {
		return 0, err
	}
	return count, nil
}

func (l *LibraryRepository) AddFakeBooks(count int) error {
	authorsCount, err := l.GetAuthorsCount()
	if err != nil {
		return err
	}

	for i := 0; i < count; i++ {
		book := model.Book{
			Title:       gofakeit.BuzzWord(),
			AuthorID:    gofakeit.Number(1, authorsCount),
			IsAvailable: true,
		}

		// добавляем книгу в таблицу
		_, err := l.db.Exec("INSERT INTO library.Books (title, author_id,is_available) VALUES ($1, $2, $3)", book.Title, book.AuthorID, book.IsAvailable)
		if err != nil {
			return err
		}
		// получаем последний вставленный book_id
		var bookID int
		err = l.db.Get(&bookID, "SELECT lastval()")
		if err != nil {
			return err
		}

		// добавляем привязку книги к автору в таблицу авторов
		_, err = l.db.Exec("INSERT INTO library.Author_Books (author_id, book_id) VALUES ($1, $2)", book.AuthorID, bookID)
		if err != nil {
			return err
		}
	}
	return nil
}
