package main

import (
	_ "library/docs"

	"github.com/jmoiron/sqlx"
	db2 "library/db"
	"library/internal/config"
	"library/internal/handlers"
	"library/internal/repository"
	"library/internal/service"
	"log"

	_ "github.com/lib/pq"
)

// @title Library APP API
// @version 1.0
// @description Library API
func main() {
	cfg := config.NewConfig()
	dbConf := config.NewDB()

	db, err := db2.NewSqlDB(*dbConf)
	if err != nil {
		log.Fatalf("Failed to connect database: %v", err)
	}
	defer func(db *sqlx.DB) {
		err := db.Close()
		if err != nil {

		}
	}(db)

	libraryRepo := repository.NewLibraryRepository(db)
	libraryService := service.NewLibraryService(*libraryRepo)

	exists, err := libraryService.CheckAuthorsExistence()
	if err != nil {
		log.Fatal(err)
	}

	if !exists {
		err = libraryService.AddFakeAuthors(10)
		if err != nil {
			log.Fatal(err)
		}
	}

	exists, err = libraryService.CheckAuthorsExistence()
	if err != nil {
		log.Fatal(err)
	}

	existsBooks, err := libraryService.CheckBooksExistence()
	if err != nil {
		log.Fatal(err)
	}

	if !existsBooks {
		err = libraryService.AddFakeBooks(100)
		if err != nil {
			log.Fatal(err)
		}
	}

	existsBooks, err = libraryService.CheckBooksExistence()
	if err != nil {
		log.Fatal(err)
	}

	apiServer := handlers.NewServer(cfg, libraryService, dbConf)

	router := handlers.SetupRouter(apiServer)
	err = router.Run()
	if err != nil {
		log.Fatalf("Failed to start API server: %v", err)
	}
}
