CREATE TABLE IF NOT EXISTS library.Authors (
                                               author_id SERIAL PRIMARY KEY,
                                               name VARCHAR(255),
                                               books INT[]
);

CREATE TABLE IF NOT EXISTS library.Users (
                                             user_id SERIAL PRIMARY KEY,
                                             name VARCHAR(255),
                                             rented_books_ids INT[],
                                             rented_books JSONB
);

CREATE TABLE IF NOT EXISTS library.Books (
                                             book_id SERIAL PRIMARY KEY,
                                             title VARCHAR(255),
                                             author_id INT REFERENCES library.Authors(author_id),
                                             is_available BOOLEAN DEFAULT TRUE
);
CREATE TABLE IF NOT EXISTS library.Borrowed_books (
                                                     borrow_id SERIAL PRIMARY KEY,
                                                     book_id INT,
                                                     user_id INT,
                                                     borrow_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
                                                     return_date TIMESTAMP,
                                                     FOREIGN KEY (book_id) REFERENCES library.Books(book_id),
                                                     FOREIGN KEY (user_id) REFERENCES library.Users(user_id)
);

CREATE TABLE IF NOT EXISTS library.Reads (
                                             read_id SERIAL PRIMARY KEY,
                                             book_id INT REFERENCES library.Books(book_id),
                                             user_id INT REFERENCES library.Users(user_id),
                                             read_date DATE DEFAULT CURRENT_DATE,
                                             FOREIGN KEY (book_id) REFERENCES library.Books(book_id),
                                             FOREIGN KEY (user_id) REFERENCES library.Users(user_id)
);

CREATE TABLE IF NOT EXISTS library.Author_Books (
                                                    author_id INT REFERENCES library.Authors(author_id),
                                                    book_id INT REFERENCES library.Books(book_id),
                                                    PRIMARY KEY (author_id, book_id)
);