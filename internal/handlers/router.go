package handlers

import (
	"context"
	"errors"
	"github.com/gin-gonic/gin"
	swaggerFiles "github.com/swaggo/files"
	ginSwagger "github.com/swaggo/gin-swagger"
	"library/internal/config"
	"library/internal/service"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

type APIServer struct {
	config         *config.Config
	libraryService *service.LibraryService
	configDB       *config.DB
}

func NewServer(config *config.Config, libraryService *service.LibraryService, cfgDB *config.DB) *APIServer {
	return &APIServer{
		config:         config,
		libraryService: libraryService,
		configDB:       cfgDB,
	}
}

func (s *APIServer) Run() {
	server := http.Server{
		Addr: s.config.HTTPAddr,
	}
	log.Printf("Listening on port %s", s.config.HTTPAddr)
	// создаем канал, который будет использоавться для сигнала завершения
	idConnClosed := make(chan struct{})
	// горутина, которая будет слушать сигналы прерываания или завершения
	go func() {
		sigint := make(chan os.Signal, 1)
		// os.Interrupt, syscall.SIGTERM - сигналы завершения программы
		signal.Notify(sigint, os.Interrupt, syscall.SIGTERM)
		<-sigint
		ctx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
		defer cancel()
		if err := server.Shutdown(ctx); err != nil {
			log.Fatalln(err)
		}
		close(idConnClosed)
	}()

	if err := server.ListenAndServe(); err != nil {
		if !errors.Is(err, http.ErrServerClosed) {
			log.Fatalln(err)
		}
	}
	<-idConnClosed
	log.Println("Server stopped")
}

func SetupRouter(s *APIServer) *gin.Engine {
	router := gin.Default()
	// обработчики запросов бд
	router.GET("/authors", s.GetAllAuthors)
	router.POST("/author", s.CreateAuthor)
	router.GET("/author/:authorID", s.GetAuthorsWithBooks)
	router.GET("/books", s.GetAllBooks)
	router.POST("/book", s.AddBook)
	router.POST("/book/:bookID/borrow/:userID", s.BorrowBook)
	router.POST("/book/return/:bookID/user/:userID", s.ReturnBook)
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))

	return router
}
