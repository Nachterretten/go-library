package service

import (
	"library/internal/model"
	"library/internal/repository"
)

type LibraryService struct {
	libraryRepo repository.LibraryRepository
}

func NewLibraryService(libraryRepo repository.LibraryRepository) *LibraryService {
	return &LibraryService{
		libraryRepo: libraryRepo,
	}
}

func (l *LibraryService) GetAllAuthors() ([]model.Author, error) {
	return l.libraryRepo.GetAllAuthors()
}

func (l *LibraryService) CreateAuthor(author model.Author) error {
	return l.libraryRepo.CreateAuthor(author)
}

func (l *LibraryService) GetBooksByAuthor(authorID int) ([]model.Book, error) {
	return l.libraryRepo.GetBooksByAuthor(authorID)
}

func (l *LibraryService) GetAllBooks() ([]model.Book, error) {
	return l.libraryRepo.GetAllBooks()
}

func (l *LibraryService) AddBook(book model.Book) error {
	return l.libraryRepo.AddBook(book)
}

func (l *LibraryService) BorrowBook(bookID, userID int) error {
	return l.libraryRepo.BorrowBook(bookID, userID)
}

func (l *LibraryService) ReturnBook(bookID, userID int) error {
	return l.libraryRepo.ReturnBook(bookID, userID)
}

func (l *LibraryService) CheckAuthorsExistence() (bool, error) {
	return l.libraryRepo.CheckAuthorsExistence()
}

func (l *LibraryService) CheckBooksExistence() (bool, error) {
	return l.libraryRepo.CheckBooksExistence()
}

func (l *LibraryService) AddFakeAuthors(count int) error {
	return l.libraryRepo.AddFakeAuthors(count)
}

func (l *LibraryService) AddFakeBooks(count int) error {
	return l.libraryRepo.AddFakeBooks(count)
}
