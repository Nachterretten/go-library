package model

import "time"

type User struct {
	UserID         int    `json:"user_id"`
	Name           string `json:"name"`
	RentedBooksIDs []int  `json:"rented_books_ids"` // Новое поле
	RentedBooks    []Book `json:"rented_books"`
}

type Author struct {
	AuthorID int    `json:"author_id"`
	Name     string `json:"name"`
	Books    []Book `json:"books"`
}

type Book struct {
	BookID      int           `json:"book_id"`
	Title       string        `json:"title"`
	AuthorID    int           `json:"author_id"`
	IsAvailable bool          `json:"is_available"`
	BorrowedBy  *BorrowedBook `json:"borrowed_by"`
}

type BorrowedBook struct {
	BorrowID   int       `json:"borrow_id"`
	Book       Book      `json:"book"`
	UserID     int       `json:"user_id"`
	BorrowDate time.Time `json:"borrow_date"`
	ReturnDate time.Time `json:"return_date"`
}

type Read struct {
	ReadID   int    `json:"read_id"`
	Book     Book   `json:"book"`
	UserID   int    `json:"user_id"`
	ReadDate string `json:"read_date"`
}
